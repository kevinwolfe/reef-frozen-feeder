#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include <time.h>
#include <esp_system.h>

#include "wifi.h"
#include "utils.h"
#include "nvm.h"
#include "hardware.h"
#include "application.h"

#include "hx711.h"
#include "ds18b20.h"

const char *app_state_text[] = 
{
  [APP_STATE_WAITING_FOR_TIME_UPDATE] = "Waiting for NTP time update",
  [APP_STATE_IDLE]                    = "Idle",
  [APP_STATE_CALIBRATE_SCALE_0]       = "Calibration Scale - Step #1",
  [APP_STATE_CALIBRATE_SCALE_1]       = "Calibrating Scale - Step #2",
  [APP_STATE_CALIBRATE_TUBING_0]      = "Calibrating Tubing - Setup",
  [APP_STATE_CALIBRATE_TUBING_1]      = "Calibrating Tubing - In Progress",
  [APP_STATE_FEEDING_SETUP_0]         = "Feeding Setup - Step #1",
  [APP_STATE_FEEDING_SETUP_1]         = "Feeding Setup - Step #2",
  [APP_STATE_FEEDING_SETUP_2]         = "Feeding Setup Confirmation",
  [APP_STATE_FEEDING_SCHEDULED]       = "Feeding Scheduled",
  [APP_STATE_FEEDING_IN_PROGRESS]     = "Feeding In Progress",
};

static app_state_t     s_app_state               = APP_STATE_WAITING_FOR_TIME_UPDATE;
static feeding_state_t s_feed_state              = { 0 };
static bool            s_manual_feed_requested   = false;
static bool            s_skip_feed_requested     = false;
static long            s_latest_scale_reading_g  = 0;


//-----------------------------------------------------------------------------
bool tubing_calibrated()
{
  return ( nvm_get_param_float(NVM_PARAM_LINE_PURGE_TIME_S) > 0 );
}

//-----------------------------------------------------------------------------
bool scale_calibrated()
{
  return ( nvm_get_param_float(NVM_PARAM_SCALE_CAL_VAL_TO_GRAMS) > 0 );
}

//-----------------------------------------------------------------------------
app_state_t get_app_state( void )
{
  return s_app_state;
}

//-----------------------------------------------------------------------------
void set_app_state( app_state_t new_state )
{
  // TODO: Queue up request, handle on thread
  s_app_state = new_state;
}

//-----------------------------------------------------------------------------
feeding_state_t get_current_feed_state()
{
  // TODO: Thread safety
  return s_feed_state;
}

//-----------------------------------------------------------------------------
void set_current_feed_state( feeding_state_t new_state )
{
  // TODO: Queue up request, handle on thread
  s_feed_state = new_state;
  nvm_set_param_blob( NVM_PARAM_FEEDING_STATE, &s_feed_state );
}

//-----------------------------------------------------------------------------
const char * get_app_state_str()
{
  return app_state_text[s_app_state];
}

//-----------------------------------------------------------------------------
void cancel_feedings()
{
  // TODO: Queue up request, handle on thread
  s_feed_state.feedings_remaining = 0;        
  nvm_set_param_blob( NVM_PARAM_FEEDING_STATE, &s_feed_state );
  set_app_state( ntp_time_is_set() ? APP_STATE_IDLE : APP_STATE_WAITING_FOR_TIME_UPDATE );
  force_mqtt_update();
}

//-----------------------------------------------------------------------------
void manual_feed_request()
{
  s_manual_feed_requested = true;
  force_mqtt_update();
}

//-----------------------------------------------------------------------------
void skip_next_feedings()
{
  s_skip_feed_requested = true;
  force_mqtt_update();
}

//-----------------------------------------------------------------------------
long get_latest_scale_reading_g()
{
  return s_latest_scale_reading_g;
}

//-----------------------------------------------------------------------------
static void application_task(void *Param)
{
  uint8_t tmp[64];
  printf( "Reef Autofeer app init\n");
  
  nvm_get_param_blob( NVM_PARAM_FEEDING_STATE, &s_feed_state );

  printf( "Feeding state:\n");
  printf( "\t%-20s %i\n", "# Feedings:",              s_feed_state.feedings_remaining );
  
  memset( tmp, 0, sizeof( tmp ) );
  add_formatted_timestamp( ( char * )tmp,             s_feed_state.next_scheduled_feed_time_s );
  printf( "\t%-20s %s\n",    "Next Feed:",            tmp );
  
  memset( tmp, 0, sizeof( tmp ) );
  add_formatted_duration_str( ( char * )tmp,          s_feed_state.time_between_feedings_s );
  printf( "\t%-20s %s\n",    "Time Between (s):",     tmp );
  
  printf( "\t%-20s %.1f\n",  "Weight Per Feed (g):",  s_feed_state.weight_per_feeding_g ); 
  printf( "\t%-20s %.1f\n",  "Line Purge Time (s):",  nvm_get_param_float(NVM_PARAM_LINE_PURGE_TIME_S) );
  printf( "\t%-20s %s\n",    "Scale Calibrated:",     scale_calibrated()  ? "Yes" : "No" );
  printf( "\t%-20s %s\n",    "Tubing Calibrated:",    tubing_calibrated() ? "Yes" : "No" );
  
  const uint32_t task_delay_ms = 250;
  
  HX711_set_scale( 4850.0 / 20 ); // 4850 counts / 20 grams = #counts / grams
  HX711_tare();
    
  while ( 1 )
  {
    // Handle init state
    if ( ( s_app_state == APP_STATE_WAITING_FOR_TIME_UPDATE ) && ntp_time_is_set() )
    {
      if ( scale_calibrated() && s_feed_state.feedings_remaining )
      {
        s_app_state = APP_STATE_FEEDING_SCHEDULED;
      }
      else
      {
        s_app_state = APP_STATE_IDLE;
      }
    }
      
    if ( s_app_state == APP_STATE_CALIBRATE_TUBING_1 )
    {
      static float reverse_pump_start_time_s;
      
      // - Run feeding pump forward 45 seconds to purge the line, fill with air
      printf("Tube cal: starting forward pumping\n");
      feed_pump_forward_request( 45, FEED_PUMP_LUDICROUS_SPEED_PCT, true );
      printf("Tube cal: finished forward pumping\n");

      // Forward purge complete, give everything a second to calm down
      vTaskDelay( 1000 / portTICK_RATE_MS);
      printf("Tube cal: starting reverse pumping\n");
      
      // Tare the scale, including the empty cup
      HX711_tare();

      // Start pumping tank water to cup, max 60 seconds
      feed_pump_reverse_request( 60, FEED_PUMP_FULL_SPEED_PCT, false );
      reverse_pump_start_time_s = system_uptime_s();

      const long scale_measurement_threshold_g = 4;
      while ( HX711_get_units( SCALE_OVERSAMPLING_CNT ) < scale_measurement_threshold_g )
      {
        vTaskDelay( 10 / portTICK_RATE_MS);
      }
      
      float new_purge_time_s = system_uptime_s() - reverse_pump_start_time_s;
      feed_pump_stop();

      nvm_set_param_float(NVM_PARAM_LINE_PURGE_TIME_S, new_purge_time_s);
      printf("Tube cal: weight change detected - line purge time: %.1f sec\n", new_purge_time_s );
      printf("Tube cal: finished\n");

      s_app_state = APP_STATE_IDLE;
    }
       
    if ( s_app_state == APP_STATE_FEEDING_SCHEDULED )
    {
      static float last_stir_time = 0;

      // Get the now's time
      time_t current_time;
      time(&current_time);
      
      if ( ( last_stir_time == 0 ) || ( ( system_uptime_s() - last_stir_time ) > STIR_PERIOD_S ) )
      {
        stir_plate_spin_request(STIR_DURATION_S, true); // Stir the food, wait until idle
        last_stir_time = system_uptime_s();
      }
      
      if ( s_skip_feed_requested )
      {
        s_skip_feed_requested = false;
        s_feed_state.next_scheduled_feed_time_s += s_feed_state.time_between_feedings_s;
        nvm_set_param_blob( NVM_PARAM_FEEDING_STATE, &s_feed_state );
      }

      bool feed_now = ( s_manual_feed_requested ) || ( ntp_time_is_set() && ( current_time >= s_feed_state.next_scheduled_feed_time_s ) );
      if ( feed_now )
      {
        printf("Feeding now!\n");
        s_app_state = APP_STATE_FEEDING_IN_PROGRESS;
        s_feed_state.feedings_remaining         -= MIN( 1, s_feed_state.feedings_remaining );
        s_feed_state.next_scheduled_feed_time_s += ( s_manual_feed_requested ? 0 : s_feed_state.time_between_feedings_s );
        s_feed_state.last_feed_time_s            = current_time;
        nvm_set_param_blob( NVM_PARAM_FEEDING_STATE, &s_feed_state );
        
        s_manual_feed_requested = false;
        
        stir_plate_spin_request(STIR_DURATION_S, true);   // Stir the food, wait until idle
        
        // Prime the food into the feed line
        printf("Priming food into feed line\n");
        vTaskDelay( 1000 / portTICK_RATE_MS);
        HX711_tare();
        
        stir_plate_spin_request( 90, false );                               // Stir the food, don't wait for idle
        feed_pump_forward_request( 90, FEED_PUMP_SLOW_SPEED_PCT, false );   // Max 90 seconds
        
        // feed_pump_busy() acts as a timeout for the while loop
        while( ( abs(HX711_get_units( 3 )) < s_feed_state.weight_per_feeding_g ) && feed_pump_busy () )
        {
          vTaskDelay( 10 / portTICK_RATE_MS);
        }
        
        feed_pump_stop();
        stir_plate_stop();
        printf("Done priming food\n");
        
        vTaskDelay( 1000 / portTICK_RATE_MS);
        
        // Run the pump to get the food into the tank.
        printf("Pushing food to tank\n");
        float tube_run_time = nvm_get_param_float(NVM_PARAM_LINE_PURGE_TIME_S);
        feed_pump_forward_request( tube_run_time - 0.1, FEED_PUMP_FULL_SPEED_PCT, true ); // -0.1 for fudge factor on measuring weights above
        
        printf("Done feeding\n");        
        vTaskDelay( 1000 / portTICK_RATE_MS);
        
        printf("Cleaning feed tube\n");
        feed_pump_reverse_request( tube_run_time, FEED_PUMP_FULL_SPEED_PCT, true );
        
        force_mqtt_update();
      }
      
      s_app_state = ( s_feed_state.feedings_remaining ? APP_STATE_FEEDING_SCHEDULED : APP_STATE_IDLE );
    }
    
    //bool button_pressed = !gpio_get_level( PIN_BUTTON );
    s_latest_scale_reading_g = HX711_get_units( SCALE_OVERSAMPLING_CNT );
    printf( "Scale reading: %li grams\n ", s_latest_scale_reading_g );
    
    vTaskDelay( task_delay_ms / portTICK_RATE_MS);
  }
}

//-----------------------------------------------------------------------------
void app_init( void )
{
  xTaskCreate( application_task, "app_task", 8192, NULL, 5, NULL);
}