#ifndef _APPLICATION_H_
#define _APPLICATION_H_

//-----------------------------------------------------------------------------
typedef enum
{
  APP_STATE_WAITING_FOR_TIME_UPDATE,      // Clears after first wifi connection and NTP
  APP_STATE_IDLE,                         // No action in progress
  APP_STATE_CALIBRATE_SCALE_0,            // User action: Clear scale, confirm
                                          // Post action: Tare Scale
  APP_STATE_CALIBRATE_SCALE_1,            // User action: Place 20 gram weight on scale, confirm
                                          // Post action: measure, set HX711 scale factor
  APP_STATE_CALIBRATE_TUBING_0,           // User action: Place empty cup on scale
  APP_STATE_CALIBRATE_TUBING_1,           // Post action: 
                                          // - Run feeding pump forward 30 seconds to purge the line, fill with air
                                          // - Run feeding pump reverse.  Measure time it takes for scale to record any water weight
                                          // .....-> Save time as 'tube purge time'
  APP_STATE_FEEDING_SETUP_0,              // User action: Place empty cup on scale
                                          // Post action: record cup weight
                                          // .....-> 'feeding weight' = Filled up weight / # of feedings
  APP_STATE_FEEDING_SETUP_1,              // User action: Fill cup with slurry, place on scale, submit how many feedings to perform
                                          // Post action: record up filled cup weight, calculate 'feeding weight'
                                          // .....-> 'feeding weight' = Filled up weight / # of feedings
  APP_STATE_FEEDING_SETUP_2,              // User action: confirm values
  APP_STATE_FEEDING_SCHEDULED,
  APP_STATE_FEEDING_IN_PROGRESS,
} app_state_t;

typedef struct
{
  uint8_t   feedings_remaining;           // #
  uint32_t  next_scheduled_feed_time_s;   // unix timestamp
  uint32_t  last_feed_time_s;             // unix timestamp
  uint32_t  time_between_feedings_s;
  float     weight_per_feeding_g;         // grams to pump per feeding
} feeding_state_t;

//-----------------------------------------------------------------------------
void            app_init( void );

app_state_t     get_app_state( void );
void            set_app_state( app_state_t new_state );
feeding_state_t get_current_feed_state( void );
void            set_current_feed_state( feeding_state_t new_state );
const char *    get_app_state_str( void );
void            cancel_feedings( void );
void            skip_next_feedings( void );
void            manual_feed_request( void );
long            get_latest_scale_reading_g( void );

bool            scale_calibrated( void );
bool            tubing_calibrated( void );

#endif