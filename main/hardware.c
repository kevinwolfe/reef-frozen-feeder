#include "hx711.h"
#include "driver/ledc.h"
#include "driver/gpio.h"

#include <u8g2.h>
#include <u8g2_esp32_hal.h>
#include "nomodoh_logo.c"

#include "utils.h"
#include "hardware.h"
#include "ds18b20.h"

#define PIN_BUTTON                (  0 )
#define PIN_HX711_SCK             (  5 )
#define PIN_HX711_DT              (  6 )

#define PIN_MOTOR_CTL_A1          ( 13 )
#define PIN_MOTOR_CTL_A2          ( 12 )
#define PIN_MOTOR_CTL_B1          (  7 )
#define PIN_MOTOR_CTL_B2          (  8 )

#define PIN_OLED_SCK              ( 17 )
#define PIN_OLED_SDA              ( 19 )
#define PIN_TEMP_SENSOR           ( 20 )
#define PIN_TEMP_SENSOR_SPARE     ( 18 )

static void _init_display(void);
static void _setup_pin_pwm( ledc_timer_t timer, ledc_channel_t channel, uint8_t gpio );
static void motor_control_task(void *Param);
static void sensors_control_task(void *Param);

typedef struct
{
  bool   stir_plate_spinning;
  bool   feed_pump_running_forward;
  bool   feed_pump_running_reverse;
  
  float  feed_pump_speed_pct;
  
  float  stir_plate_spin_request_s;
  float  feed_pump_run_forward_request_s;
  float  feed_pump_run_reverse_request_s;
} motors_state_t;

static u8g2_t           oled_u8g2;
static motors_state_t   motors = { 0 };

static float           s_latest_temp_reading_f   = 0;

//static bool           temp_sensor_identified = false;
//static DeviceAddress  temp_sensor_address[1];


//-----------------------------------------------------------------------------
static void _init_display()
{
	u8g2_esp32_hal_t u8g2_esp32_hal = U8G2_ESP32_HAL_DEFAULT;
	u8g2_esp32_hal.sda = PIN_OLED_SDA;
	u8g2_esp32_hal.scl = PIN_OLED_SCK;
	u8g2_esp32_hal_init( u8g2_esp32_hal );
	
	u8g2_Setup_sh1106_i2c_128x64_noname_f( &oled_u8g2, U8G2_R0, u8g2_esp32_i2c_byte_cb, u8g2_esp32_gpio_and_delay_cb );  // init oled_u8g2 structure

	u8x8_SetI2CAddress( &oled_u8g2.u8x8, 0x78);
	u8g2_InitDisplay(   &oled_u8g2 );       // send init sequence to the display, display is in sleep mode after this,
	u8g2_SetPowerSave(  &oled_u8g2, U8G2_POWER_SAVE_SCREEN_ON );   // wake up display

	u8g2_ClearBuffer(   &oled_u8g2 );
	u8g2_SendBuffer(    &oled_u8g2 );
}

//-----------------------------------------------------------------------------
static void _setup_pin_pwm( ledc_timer_t timer, ledc_channel_t channel, uint8_t gpio )
{
  ledc_channel_config_t ledc_channel = {0};
  ledc_channel.gpio_num = gpio;
  ledc_channel.speed_mode = LEDC_LOW_SPEED_MODE;
  ledc_channel.channel = channel;
  ledc_channel.intr_type = LEDC_INTR_DISABLE;
  ledc_channel.timer_sel = timer;
  ledc_channel.duty = 0;

  ledc_timer_config_t ledc_timer = {0};
  ledc_timer.speed_mode = LEDC_LOW_SPEED_MODE;
  ledc_timer.duty_resolution = LEDC_TIMER_10_BIT;
  ledc_timer.timer_num = timer;
  ledc_timer.freq_hz = 25000;
  
  ledc_channel_config(&ledc_channel);
  ledc_timer_config(&ledc_timer);
}

//-----------------------------------------------------------------------------
void set_screen_text( const char *line1, const char *line2, const char *line3, const char *line4 )
{
  u8g2_ClearBuffer( &oled_u8g2 );

  u8g2_SetFont( &oled_u8g2, u8g2_font_helvR10_tr );
  u8g2_DrawStr( &oled_u8g2, 0, 15, line1 );

  u8g2_SetFont( &oled_u8g2, u8g2_font_timR08_tr  );
  u8g2_DrawStr( &oled_u8g2, 0, 35, line2 );
  u8g2_DrawStr( &oled_u8g2, 0, 49, line3 );
  u8g2_DrawStr( &oled_u8g2, 0, 63, line4 );
  
  u8g2_DrawXBM( &oled_u8g2, 128-NOMODOH_LOGO_WIDTH, 0, NOMODOH_LOGO_WIDTH, NOMODOH_LOGO_HEIGHT, nomodoh_logo);

  u8g2_SendBuffer( &oled_u8g2 );
}

//-----------------------------------------------------------------------------
void set_stir_plate_speed(float speed_pct)
{
  static float last_set_speed = -1;
  if ( speed_pct != last_set_speed )
  {
    printf("Setting stir plate speed to %i%%\n", (int)(100 * speed_pct));
    uint32_t pwm_duty = (uint32_t)(speed_pct * ((1 << LEDC_TIMER_10_BIT) - 1));
    ledc_set_duty(LEDC_LOW_SPEED_MODE, LEDC_CHANNEL_1, pwm_duty);
    ledc_update_duty(LEDC_LOW_SPEED_MODE, LEDC_CHANNEL_1);
    last_set_speed = speed_pct;
  }
}

//-----------------------------------------------------------------------------
void set_forward_pump_speed(float speed_pct)
{
  static float last_set_speed = -1;
  if ( speed_pct != last_set_speed )
  {
    printf("Setting forward pump speed to %i%%\n", (int)(100 * speed_pct));
    uint32_t pwm_duty = (uint32_t)(speed_pct * ((1 << LEDC_TIMER_10_BIT) - 1));
    ledc_set_duty(LEDC_LOW_SPEED_MODE, LEDC_CHANNEL_2, pwm_duty);
    ledc_update_duty(LEDC_LOW_SPEED_MODE, LEDC_CHANNEL_2);
    last_set_speed = speed_pct;
  }
}

//-----------------------------------------------------------------------------
void set_reverse_pump_speed(float speed_pct)
{
  static float last_set_speed = -1;
  if ( speed_pct != last_set_speed )
  {
    printf("Setting reverse pump speed to %i%%\n", (int)(100 * speed_pct));
    uint32_t pwm_duty = (uint32_t)(speed_pct * ((1 << LEDC_TIMER_10_BIT) - 1));
    ledc_set_duty(LEDC_LOW_SPEED_MODE, LEDC_CHANNEL_3, pwm_duty);
    ledc_update_duty(LEDC_LOW_SPEED_MODE, LEDC_CHANNEL_3);
    last_set_speed = speed_pct;
  }
}

//-----------------------------------------------------------------------------
bool stir_plate_busy(void)
{
  return ( ( motors.stir_plate_spin_request_s > 0 ) || motors.stir_plate_spinning );
}

//-----------------------------------------------------------------------------
bool feed_pump_busy(void)
{
  return ( ( motors.feed_pump_run_forward_request_s > 0 ) || motors.feed_pump_running_forward ||
           ( motors.feed_pump_run_reverse_request_s > 0 ) || motors.feed_pump_running_reverse );
}

//-----------------------------------------------------------------------------
void stir_plate_spin_request(float duration_s, bool wait_until_idle)
{
  motors.stir_plate_spin_request_s = duration_s;
  while ( wait_until_idle && stir_plate_busy() )
  {
    vTaskDelay( 10 / portTICK_RATE_MS);
  }
}

//-----------------------------------------------------------------------------
void feed_pump_forward_request(float duration_s, float speed_pct, bool wait_until_idle)
{
  motors.feed_pump_speed_pct             = speed_pct;
  motors.feed_pump_run_forward_request_s = duration_s;
  while ( wait_until_idle && feed_pump_busy() )
  {
    vTaskDelay( 10 / portTICK_RATE_MS);
  }
}

//-----------------------------------------------------------------------------
void feed_pump_reverse_request(float duration_s, float speed_pct, bool wait_until_idle)
{
  motors.feed_pump_speed_pct             = speed_pct;
  motors.feed_pump_run_reverse_request_s = duration_s;
  while ( wait_until_idle && feed_pump_busy() )
  {
    vTaskDelay( 10 / portTICK_RATE_MS);
  }
}

//-----------------------------------------------------------------------------
void feed_pump_stop(void)
{
  motors.feed_pump_run_forward_request_s = -1;
  motors.feed_pump_run_reverse_request_s = -1;
}

//-----------------------------------------------------------------------------
void stir_plate_stop(void)
{
  motors.stir_plate_spin_request_s = -1;
}

//-----------------------------------------------------------------------------
static void motor_control_task(void *Param)
{
  printf( "Motor Control task starting\n" );
  memset( &motors, 0, sizeof( motors ) );

  const uint32_t task_delay_ms = 50;
  
  const float stir_plate_low_speed_pwm_duty    = 0.35;
  const float stir_plate_high_speed_run_time_s = 0.75;
  float stir_plate_high_speed_shutoff_time_s   = 0;  
  float stir_plate_low_speed_shutoff_time_s    = 0;  
  float feed_pump_forward_shutoff_time_s       = 0;
  float feed_pump_reverse_shutoff_time_s       = 0;

  motors.feed_pump_speed_pct                   = 1; // Default to full speed
  
  while ( 1 )
  {
    float now_s = system_uptime_s();

    // Handle requests
    if ( motors.stir_plate_spin_request_s )
    {
      stir_plate_high_speed_shutoff_time_s = now_s + stir_plate_high_speed_run_time_s;
      stir_plate_low_speed_shutoff_time_s  = now_s + motors.stir_plate_spin_request_s;
      motors.stir_plate_spinning           = true;
      motors.stir_plate_spin_request_s     = 0;
    }   
    
    if ( motors.feed_pump_run_forward_request_s )
    {
      feed_pump_forward_shutoff_time_s = now_s + motors.feed_pump_run_forward_request_s;
      motors.feed_pump_running_forward       = true;
      motors.feed_pump_run_forward_request_s = 0;
    }
    
    if ( motors.feed_pump_run_reverse_request_s )
    {
      feed_pump_reverse_shutoff_time_s = now_s + motors.feed_pump_run_reverse_request_s;
      motors.feed_pump_running_reverse       = true;
      motors.feed_pump_run_reverse_request_s = 0;
    }
    
    // Drive the stir plate
    if ( now_s < stir_plate_high_speed_shutoff_time_s )
    {
      set_stir_plate_speed( 1 );
    }
    else if ( now_s < stir_plate_low_speed_shutoff_time_s )
    {
      set_stir_plate_speed( stir_plate_low_speed_pwm_duty );
    }
    else
    {
      set_stir_plate_speed( 0 );
      motors.stir_plate_spinning = false;
    }
    
    // Drive the pumps
    set_forward_pump_speed( ( now_s < feed_pump_forward_shutoff_time_s ) ? motors.feed_pump_speed_pct : 0 );
    set_reverse_pump_speed( ( now_s < feed_pump_reverse_shutoff_time_s ) ? motors.feed_pump_speed_pct : 0 );

    motors.feed_pump_running_forward = ( now_s < feed_pump_forward_shutoff_time_s );
    motors.feed_pump_running_reverse = ( now_s < feed_pump_reverse_shutoff_time_s );

    vTaskDelay( task_delay_ms / portTICK_RATE_MS);
  }
}

//-----------------------------------------------------------------------------
static void sensors_control_task(void *Param)
{
  // Create a 1-Wire bus, using the RMT timeslot driver
  OneWireBus * owb;
  owb_rmt_driver_info rmt_driver_info;
  owb = owb_rmt_initialize(&rmt_driver_info, PIN_TEMP_SENSOR, RMT_CHANNEL_1, RMT_CHANNEL_0);
  owb_use_crc(owb, true);  // enable CRC check for ROM code

  OneWireBus_SearchState  search_state = {0};
  bool                    found = false;
  DS18B20_Info            device;
  
  TickType_t last_wake_time = xTaskGetTickCount();

  while (1)
  {
      if ( !found )
      {
        owb_search_first(owb, &search_state, &found);
        if ( found )
        {
            printf("Single device optimisations enabled\n");
            ds18b20_init_solo(&device, owb);          // only one device on bus
            ds18b20_use_crc(&device, true);           // enable CRC check on all reads
            ds18b20_set_resolution(&device, DS18B20_RESOLUTION_12_BIT);
        }
      }
      
      if ( found )
      {
        ds18b20_convert_all(owb);

        // In this application all devices use the same resolution,
        // so use the first device to determine the delay
        ds18b20_wait_for_conversion(&device);

        // Read the results immediately after conversion otherwise it may fail
        // (using printf before reading may take too long)
        float reading_c = 0;
        ds18b20_read_temp(&device, &reading_c);

        s_latest_temp_reading_f = ( reading_c * 9 / 5 ) + 32;
      }

      #define SAMPLE_PERIOD        (1000)   // milliseconds
      vTaskDelayUntil(&last_wake_time, SAMPLE_PERIOD / portTICK_PERIOD_MS);
  }
}

//-----------------------------------------------------------------------------
float get_latest_temp_reading_f()
{
  return s_latest_temp_reading_f;
}

//-----------------------------------------------------------------------------
void hardware_init()
{
  gpio_config_t io_conf;
  io_conf.pin_bit_mask = ( 1ULL<< PIN_BUTTON );
  io_conf.mode = GPIO_MODE_INPUT;
  io_conf.pull_up_en = 1;
  io_conf.pull_down_en = 0;
  gpio_config(&io_conf);
  
  io_conf.pin_bit_mask = ( 1ULL << PIN_HX711_DT ) | 
                         ( 1ULL << PIN_MOTOR_CTL_A1 ) | ( 1ULL << PIN_MOTOR_CTL_A2 ) | 
                         ( 1ULL << PIN_MOTOR_CTL_B1 ) | ( 1ULL << PIN_MOTOR_CTL_B2 );
  io_conf.mode = GPIO_MODE_OUTPUT;
  gpio_set_level( PIN_HX711_DT, 1 );
  
  gpio_set_level( PIN_MOTOR_CTL_A1, 0 );
  gpio_set_level( PIN_MOTOR_CTL_A2, 0 );
  gpio_set_level( PIN_MOTOR_CTL_B1, 0 );
  gpio_set_level( PIN_MOTOR_CTL_B2, 0 );

  gpio_config(&io_conf);
  
  _setup_pin_pwm( LEDC_TIMER_1, LEDC_CHANNEL_1, PIN_MOTOR_CTL_B1 );
  _setup_pin_pwm( LEDC_TIMER_2, LEDC_CHANNEL_2, PIN_MOTOR_CTL_A1 );
  _setup_pin_pwm( LEDC_TIMER_3, LEDC_CHANNEL_3, PIN_MOTOR_CTL_A2 );
  
  HX711_init( PIN_HX711_DT, PIN_HX711_SCK, eGAIN_64 );

  _init_display();
  
  xTaskCreate( motor_control_task, "motors_task", 8192, NULL, 5, NULL);
  xTaskCreate( sensors_control_task, "sensors_task", 1024*3, NULL, 5, NULL);
}