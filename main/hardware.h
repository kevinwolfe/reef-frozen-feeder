#ifndef _HARDWARE_H_
#define _HARDWARE_H_

#define FEED_PUMP_SLOW_SPEED_PCT       (0.8)
#define FEED_PUMP_FULL_SPEED_PCT       (0.9)
#define FEED_PUMP_LUDICROUS_SPEED_PCT  (1.0)

#define SCALE_OVERSAMPLING_CNT   25       // 25 readings averaged per reading

#define STIR_PERIOD_S            ( 15 * 60 )      // Time between food stirs, 15 minutes
#define STIR_DURATION_S          ( 30 )           // Amount of time to stir the food


void  hardware_init();

void  set_screen_text( const char *line1, const char *line2, const char *line3, const char *line4 );
void  stir_plate_spin_request(float duration_s, bool wait_until_idle);
void  feed_pump_forward_request(float duration_s, float speed_pct, bool wait_until_idle);
void  feed_pump_reverse_request(float duration_s, float speed_pct, bool wait_until_idle);
void  feed_pump_stop(void);
void  set_stir_plate_speed(float speed_pct);
void  stir_plate_stop(void);
void  set_forward_pump_speed(float speed_pct);
void  set_reverse_pump_speed(float speed_pct);
bool  stir_plate_busy(void);
bool  feed_pump_busy(void);
float get_latest_temp_reading_f(void);

#endif
