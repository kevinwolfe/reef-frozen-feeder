#include <esp_wifi.h>

#include <esp_log.h>
#include <esp_system.h>
#include <sys/param.h>

#include <time.h>
#include <sys/time.h>
#include <string.h>

#include "utils.h"
#include "hardware.h"
#include "wifi.h"


#include "nvm.h"
#include "application.h"

//-----------------------------------------------------------------------------
void app_main( void )
{ 
  printf( "****************************\n" );
  
  esp_event_loop_create_default();
  
  setenv("TZ", "PST8PDT,M3.2.0,M11.1.0", 1);
  tzset();
  
  hardware_init();
  nvm_init();
  wifi_init();
  app_init();
  
  nvm_set_param_int32( NVM_PARAM_RESET_COUNTER, nvm_get_param_int32(NVM_PARAM_RESET_COUNTER) + 1 );
  
  char screen_text[3][36];
  bool screen_toggle = false;
  const float screen_toggle_rate_s = 4.0;
  float screen_last_update_s = 0;
  
  const uint32_t task_delay_ms = 2000;

  while(1)
  {
    vTaskDelay( task_delay_ms / portTICK_RATE_MS);    
    fflush(stdout);
    
    float now_s = system_uptime_s();

    if ( ( now_s - screen_last_update_s ) >= screen_toggle_rate_s )
    {
      screen_last_update_s = now_s;
      memset( screen_text, 0, 3 * 24 );

      feeding_state_t current_feed_state = get_current_feed_state();
      if ( current_feed_state.feedings_remaining )
      {
        sprintf( screen_text[0], "%i feeds remaining", current_feed_state.feedings_remaining );
        sprintf( screen_text[1], "Next feed: " );
        add_formatted_timestamp( &screen_text[1][11], current_feed_state.next_scheduled_feed_time_s );
      }
      else
      {
        sprintf( screen_text[0], "No scheduled feedings" );
        sprintf( screen_text[1], "Next feed: Never" );
      }
      
      sprintf( screen_text[2], "Last feed: " );
      if ( current_feed_state.last_feed_time_s > 0 )
      {
        add_formatted_timestamp( &screen_text[2][11], current_feed_state.last_feed_time_s );
      }
      else
      {
        sprintf( &screen_text[2][11], "Never" );
      }

      screen_toggle = !screen_toggle;
      set_screen_text( "Frozen Feeder", 
                        screen_toggle ? screen_text[0] : get_app_state_str(), 
                        screen_toggle ? screen_text[1] : get_system_time_str(), 
                        screen_toggle ? screen_text[2] : get_ip_addr_str() );
    }
  }
}
