#include <esp_timer.h>
#include <time.h>

#include "utils.h"

//-----------------------------------------------------------------------------
float system_uptime_s( void )
{
  // esp_timer_get_time returns time in uSec
  return (float)(((double)esp_timer_get_time()) / 1000000);
}

//-----------------------------------------------------------------------------
const char * get_system_time_str()
{
  static char strftime_buf[48];
  
  // wait for time to be set
  time_t now = 0;
  struct tm timeinfo = { 0 };

  time(&now);
  localtime_r(&now, &timeinfo);
  strftime(strftime_buf, sizeof(strftime_buf), "%I:%M %p, %x", &timeinfo);
  return strftime_buf;
}

//-----------------------------------------------------------------------------
uint16_t add_formatted_duration_str( char *p_buffer, uint32_t duration_s )
{
  uint32_t s = duration_s;
  uint16_t h = (s/3600); 
  uint16_t m = (s -(3600*h))/60;
  s -= (60*60*h)+(m*60);

  uint16_t retv = 0;
  if ( h )
    retv += sprintf( p_buffer + retv, "%i hours%s", h, ( m || s ) ? ", " : "" );
  if ( m )
    retv += sprintf( p_buffer + retv, "%i minutes%s", m, ( s ) ? ", " : "" );
  if ( s )
    retv += sprintf( p_buffer + retv, "%i seconds", s );

  return retv;
}

//-----------------------------------------------------------------------------
uint16_t add_formatted_timestamp( char *p_buffer, uint32_t unix_time_s )
{
  char tmp[48] = { 0 };
  
  struct tm *timeinfo;
  time_t t = unix_time_s;
  timeinfo = localtime( &t );
  
  strftime(tmp, sizeof(tmp), "%I:%M %p, %x", timeinfo);
  return sprintf( p_buffer, "%s", tmp );
}