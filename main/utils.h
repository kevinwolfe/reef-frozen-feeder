#ifndef _UTILS_H_
#define _UTILS_H_

#ifndef MIN
#define MIN(a,b) (((a) < (b)) ? (a) : (b))
#endif

#ifndef MAX
#define MAX(a,b) (((a) > (b)) ? (a) : (b))
#endif

float         system_uptime_s( void );
const char *  get_system_time_str();
uint16_t      add_formatted_duration_str( char *p_buffer, uint32_t duration_s );
uint16_t      add_formatted_timestamp( char *p_buffer, uint32_t unix_time_s );

#endif
