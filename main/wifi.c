#include <string.h>
#include <stdio.h>
#include <stdint.h>

#include <esp_tls_crypto.h>
#include <esp_http_server.h>
#include <esp_event.h>
#include <esp_system.h>
#include <esp_app_format.h>
#include <esp_netif.h>
#include <esp_eth.h>
#include <esp_ota_ops.h>
#include <mqtt_client.h>
#include <esp_sntp.h>
#include <esp_http_server.h>

#include <esp_flash_partitions.h>
#include <esp_partition.h>
#include <protocol_examples_common.h>

#include "utils.h"
#include "wifi.h"
#include "nvm.h"
#include "hx711.h"
#include "hardware.h"
#include "application.h"

//-----------------------------------------------------------------------------
typedef struct
{
  const char *username;
  const char *password;
} basic_auth_info_t;

//-----------------------------------------------------------------------------
static bool s_mqtt_subscribed = false;
static bool s_wifi_connected = false;
static bool s_ntp_time_set = false;
static char s_ip_addr_str[16] = "0.0.0.0";
static char s_scratch_buffer[1024 * 3];

static feeding_state_t s_pending_feeding_state = { 0 };
static bool s_force_mqtt_updates = false;

//-----------------------------------------------------------------------------
static char *_http_auth_basic( const char *username, const char *password );
static uint16_t _html_add_system_state( char *p_buffer );
static uint16_t _html_add_submit_button( char *p_buffer, char *submit_value, char *button_text );
static void _get_app_html( char *p_buffer );
static esp_err_t _http_app_get_handler( httpd_req_t *req );
static esp_err_t _http_app_post_handler( httpd_req_t *req );
static esp_err_t _http_ota_get_handler( httpd_req_t *req );
static esp_err_t _http_ota_post_handler( httpd_req_t *req );
static httpd_handle_t _start_webserver( void );
static void _connect_handler( void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data );
static void _disconnect_handler( void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data );
static void _time_sync_notification_cb(struct timeval *tv);
static void _wifi_task( void *Param );
static void _ota_task(void *Param);

//-----------------------------------------------------------------------------
void force_mqtt_update()
{
  s_force_mqtt_updates = true;
}

//-----------------------------------------------------------------------------
bool ntp_time_is_set()
{
  return s_ntp_time_set;
}

//-----------------------------------------------------------------------------
const char *get_ip_addr_str()
{
  return ( const char *)s_ip_addr_str;
}

//-----------------------------------------------------------------------------
static void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data)
{
    esp_mqtt_event_handle_t event = event_data;
    esp_mqtt_client_handle_t client = event->client;
    switch ((esp_mqtt_event_id_t)event_id)
    {
      case MQTT_EVENT_CONNECTED:
          printf( "MQTT Connected to server\n" );
          esp_mqtt_client_subscribe(client, "reef/frozen_feeder/requests", 0);
          break;

      case MQTT_EVENT_DISCONNECTED:
          s_mqtt_subscribed = false;
          printf( "MQTT Server Disconnect!\n");
          break;

      case MQTT_EVENT_DATA:
          event->data[event->data_len] = 0;
          printf("Feeder request message data: %s\n", event->data);
          if (strcmp( event->data, "skip_feeding") == 0 )
          {
            skip_next_feedings();
          }
          if (strcmp( event->data, "cancel_feeding") == 0 )
          {
            cancel_feedings();
          }
          if (strcmp( event->data, "feed_now") == 0 )
          {
            manual_feed_request();
          }

          break;

      case MQTT_EVENT_ERROR:
          printf( "MQTT Event Error!\n" );
          break;

      case MQTT_EVENT_SUBSCRIBED:
        s_mqtt_subscribed = true;
        break;
        
      case MQTT_EVENT_UNSUBSCRIBED:
        s_mqtt_subscribed = false;
        break;
      
      case MQTT_EVENT_PUBLISHED:
      default:
        break;
    }
    fflush(stdout);
}

//-----------------------------------------------------------------------------
static char *_http_auth_basic( const char *username, const char *password )
{
  int out;
  char user_info[128];
  static char digest[512];
  size_t n = 0;
  sprintf( user_info, "%s:%s", username, password );

  esp_crypto_base64_encode( NULL, 0, &n, ( const unsigned char * )user_info, strlen( user_info ) );

  // 6: The length of the "Basic " string
  // n: Number of bytes for a base64 encode format
  // 1: Number of bytes for a reserved which be used to fill zero
  if ( sizeof( digest ) > ( 6 + n + 1 ) )
  {
    strcpy( digest, "Basic " );
    esp_crypto_base64_encode( ( unsigned char * )digest + 6, n, ( size_t * )&out, ( const unsigned char * )user_info, strlen( user_info ) );
  }

  return digest;
}

//-----------------------------------------------------------------------------
static uint16_t _html_add_system_state( char *p_buffer )
{ 
  uint16_t retv = 0;
  retv += sprintf( p_buffer + retv, "<br>" );
  retv += sprintf( p_buffer + retv, "<h2>State: %s<br></h2>", get_app_state_str() );
  retv += sprintf( p_buffer + retv, "<br>" );
  
  if ( !scale_calibrated() || !tubing_calibrated() )
  {
    if ( get_app_state() == APP_STATE_IDLE )
    {
      if ( !scale_calibrated() )
        retv += sprintf( p_buffer + retv, "Scale Calibration Required<br>" );
      if ( !tubing_calibrated() )
        retv += sprintf( p_buffer + retv, "Tubing Calibration Required<br>" );
    }
  }
  else
  {
    retv += sprintf( p_buffer + retv, "System Calibrated<br>" );

    feeding_state_t current_feed_state = get_current_feed_state();
    if ( current_feed_state.feedings_remaining )
    {
      retv += sprintf( p_buffer + retv, "%i remaining<br>", current_feed_state.feedings_remaining );
      
      retv += sprintf( p_buffer + retv, "Time between feedings: " );
      retv += add_formatted_duration_str( p_buffer + retv, current_feed_state.time_between_feedings_s );
      retv += sprintf( p_buffer + retv, "<br>" );
      
      retv += sprintf( p_buffer + retv, "Next feeding at: " );
      retv += add_formatted_timestamp( p_buffer + retv, current_feed_state.next_scheduled_feed_time_s );
      retv += sprintf( p_buffer + retv, "<br>" );
    }
    else
    {
      retv += sprintf( p_buffer + retv, "<h2>No Feedings Scheduled</h2><br>" );
    }
  }

  return retv;
}

//-----------------------------------------------------------------------------
static uint16_t _html_add_submit_button( char *p_buffer, char *submit_value, char *button_text )
{
  return sprintf( p_buffer , 
                  "<button type=\"submit\" style=\"margin:5px;\" name=\"Action\" value=\"%s\" />%s</button>", 
                  submit_value,
                  button_text );
}

//-----------------------------------------------------------------------------
static void _get_app_html( char *p_buffer )
{
  esp_app_desc_t app_desc;
  esp_ota_get_partition_description(esp_ota_get_running_partition(), &app_desc);
  
  p_buffer += sprintf( p_buffer, "<center><h1>Auto Frozen Feeder</h1>" );
  p_buffer += sprintf( p_buffer, "FW Version: %s %s<br>", app_desc.date, app_desc.time );
  p_buffer += sprintf( p_buffer, "System Time: %s<br>", get_system_time_str() );
  p_buffer += sprintf( p_buffer, "Scale Reading: %li grams<br>", get_latest_scale_reading_g() );
  p_buffer += sprintf( p_buffer, "Temperature Reading: %0.1fF<br>", get_latest_temp_reading_f() );
  p_buffer += sprintf( p_buffer, "<hr>" );
  p_buffer += _html_add_system_state( p_buffer );
  p_buffer += sprintf( p_buffer, "<hr>" );

  // Form Submit section
  p_buffer += sprintf( p_buffer, "<form action=\"/\" method=\"post\">" );
 
  app_state_t     current_app_state  = get_app_state();
  feeding_state_t current_feed_state = get_current_feed_state();
  switch( current_app_state )
  {
    case APP_STATE_IDLE:
    case APP_STATE_WAITING_FOR_TIME_UPDATE:
    case APP_STATE_FEEDING_SCHEDULED:  
    case APP_STATE_FEEDING_IN_PROGRESS:
      if ( current_app_state == APP_STATE_IDLE )
      {
        p_buffer += _html_add_submit_button( p_buffer, "tare_scale", "Tare Scale" );
        p_buffer += _html_add_submit_button( p_buffer, "cal_scale", "Calibrate Scale" );
        p_buffer += _html_add_submit_button( p_buffer, "cal_tubing", "Calibrate Tubing" );
        if ( scale_calibrated() && tubing_calibrated() )
        {
          p_buffer += _html_add_submit_button( p_buffer, "setup_feeding", "Setup Feedings" );
        }
      }

      p_buffer += _html_add_submit_button( p_buffer, "stir_food", "Run the stir plate" );
      p_buffer += _html_add_submit_button( p_buffer, "purge_forward", "Purge food tube forward" );
      p_buffer += _html_add_submit_button( p_buffer, "purge_reverse", "Purge food tube reverse" );
      p_buffer += _html_add_submit_button( p_buffer, "reboot", "Reboot MCU" );
      if ( current_feed_state.feedings_remaining )
      {
        p_buffer += sprintf( p_buffer, "<br>" );
        p_buffer += _html_add_submit_button( p_buffer, "feed_now", "Feed Now" );
        p_buffer += _html_add_submit_button( p_buffer, "skip_feeding", "Skip next feedings" );
        p_buffer += _html_add_submit_button( p_buffer, "cancel_feeding", "Cancel all feedings" );
      }
      p_buffer += sprintf( p_buffer, "<meta http-equiv=\"refresh\" content=\"30\" />" );
      break;

    case APP_STATE_CALIBRATE_SCALE_0:
      p_buffer += sprintf( p_buffer, "<b>USER ACTION:</b> Ensure nothing is on the scale<br>" );
      p_buffer += _html_add_submit_button( p_buffer, "next", "Continue calibration" );
      p_buffer += _html_add_submit_button( p_buffer, "cancel", "Cancel calibration" );
      break;

    case APP_STATE_CALIBRATE_SCALE_1:
      p_buffer += sprintf( p_buffer, "<b>USER ACTION:</b> Place the 20 gram calibration weight on the scale<br>" );
      p_buffer += _html_add_submit_button( p_buffer, "finish", "Finish calibration" );
      p_buffer += _html_add_submit_button( p_buffer, "cancel", "Cancel calibration" );
      break;

    case APP_STATE_CALIBRATE_TUBING_0:
      p_buffer += sprintf( p_buffer, "<table><tr><td>" );
      p_buffer += sprintf( p_buffer, "<center><b>USER ACTION:</b><p></center>" );
      p_buffer += sprintf( p_buffer, "<li>Place the EMPTY food container on the scale<br>" );
      p_buffer += sprintf( p_buffer, "<li>Submerge the FORWARD end of the feed line into tank<br>" );
      p_buffer += sprintf( p_buffer, "<li>Place the REVERSE end of the feed line into empty food container<br>" );
      p_buffer += sprintf( p_buffer, "<li>Install chiller lid<br>" );
      p_buffer += sprintf( p_buffer, "</td></tr></table><p>" );
      p_buffer += sprintf( p_buffer, "Note: The pumps will run upon continuing<br>" );
      p_buffer += _html_add_submit_button( p_buffer, "next", "Continue Calibration" );
      p_buffer += _html_add_submit_button( p_buffer, "cancel", "Cancel calibration" );
      break;
      
    case APP_STATE_CALIBRATE_TUBING_1:
      p_buffer += sprintf( p_buffer, "Calibration in Progress - Please wait (page will reload)<p>" );
      p_buffer += sprintf( p_buffer, "<meta http-equiv=\"refresh\" content=\"4\" />" );
      p_buffer += _html_add_submit_button( p_buffer, "cancel", "Cancel calibration" );
      break;

    case APP_STATE_FEEDING_SETUP_0:
      p_buffer += sprintf( p_buffer, "<b>USER ACTION:</b> Place empty cup & stir bar on scale <p>" );
      p_buffer += _html_add_submit_button( p_buffer, "next", "Continue setup" );
      p_buffer += _html_add_submit_button( p_buffer, "cancel", "Cancel setup" );
      break;

    case APP_STATE_FEEDING_SETUP_1:
      p_buffer += sprintf( p_buffer, "<b>USER ACTION:</b> Fill food cup with food slurry, place on scale<p>" );
      p_buffer += sprintf( p_buffer, "Number of feedings to schedule: <input type=\"text\" name=\"FeedCnt\" oninput=\"this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\\..*?)\\..*/g, '$1').replace(/^0[^.]/, '0');\"/><br>" );     
      p_buffer += sprintf( p_buffer, "Note: Feeding scheduled after 4:00 PM will start on the day after<p>" );
      p_buffer += _html_add_submit_button( p_buffer, "next", "Continue setup" );
      p_buffer += _html_add_submit_button( p_buffer, "cancel", "Cancel setup" );
      break;
     
    case APP_STATE_FEEDING_SETUP_2:
      p_buffer += sprintf( p_buffer, "<table><tr><td>" );
      p_buffer += sprintf( p_buffer, "<center><b>USER ACTION:</b><p></center>" );
      p_buffer += sprintf( p_buffer, "<li>Submerge the FORWARD end of the feed line into tank<br>" );
      p_buffer += sprintf( p_buffer, "<li>Place the REVERSE end of the feed line into empty food container<br>" );
      p_buffer += sprintf( p_buffer, "<li>Confirm input</b>:<br>" );
      p_buffer += sprintf( p_buffer, "</td></tr></table><p>" );
      p_buffer += sprintf( p_buffer, "# feedings: %i<br>", s_pending_feeding_state.feedings_remaining );
      p_buffer += sprintf( p_buffer, "Weight/feeding: %.1f grams<br>", s_pending_feeding_state.weight_per_feeding_g );
      p_buffer += sprintf( p_buffer, "Time between feedings: " );
      p_buffer += add_formatted_duration_str( p_buffer, s_pending_feeding_state.time_between_feedings_s );
      p_buffer += sprintf( p_buffer, "<br>" );
      p_buffer += sprintf( p_buffer, "First scheduled feeding: " );
      p_buffer += add_formatted_timestamp( p_buffer, s_pending_feeding_state.next_scheduled_feed_time_s );
      p_buffer += sprintf( p_buffer, "<br>" );
      p_buffer += _html_add_submit_button( p_buffer, "finish", "Confirm settings" );
      p_buffer += _html_add_submit_button( p_buffer, "cancel", "Cancel calibration" );
      break;

    default:
      break;
  }

  p_buffer += sprintf( p_buffer, "</form>" );
}

//-----------------------------------------------------------------------------
static esp_err_t _http_app_get_handler( httpd_req_t *req )
{
  _get_app_html( s_scratch_buffer );

  httpd_resp_set_status( req, HTTPD_200 );
  httpd_resp_set_hdr( req, "Connection", "keep-alive" );
  httpd_resp_send( req, s_scratch_buffer, strlen( s_scratch_buffer ) );
  return ESP_OK;
}

//-----------------------------------------------------------------------------
static esp_err_t _http_app_post_handler( httpd_req_t *req )
{
  printf( "App post received\n" );
  fflush( stdout );
  
  char post_data[256] = { 0 };
  //httpd_req_recv( req, post_data, sizeof( post_data ) );
  httpd_req_recv(req, post_data, MIN( sizeof(post_data) - 1, req->content_len));
  printf( "Received: %s\n", post_data);
  
  switch (get_app_state())
  {
    // Response: Action=[tare_scale|cal_scale|cal_tubing|stir_food|purge_forward|purge_reverse|setup_feeding|cancel_feeding|skip_feeding|feed_now|reboot]
    case APP_STATE_IDLE:
    case APP_STATE_WAITING_FOR_TIME_UPDATE:
    case APP_STATE_FEEDING_SCHEDULED:  
    case APP_STATE_FEEDING_IN_PROGRESS:
      
      if ( strcmp( post_data, "Action=tare_scale" ) == 0 )
      {
        HX711_tare();
      }
      else if ( strcmp( post_data, "Action=cal_scale" ) == 0 )
      {
        set_app_state(APP_STATE_CALIBRATE_SCALE_0);
      }
      else if ( strcmp( post_data, "Action=cal_tubing" ) == 0 )
      {
        set_app_state(APP_STATE_CALIBRATE_TUBING_0);
      }
      else if ( strcmp( post_data, "Action=stir_food" ) == 0 )
      {
        stir_plate_spin_request(5.0, false);         // Spin for 5 seconds
      }
      else if ( strcmp( post_data, "Action=purge_forward" ) == 0 )
      {
        feed_pump_forward_request(5.0, FEED_PUMP_FULL_SPEED_PCT, false);    // Full speed for 5 seconds
      }
      else if ( strcmp( post_data, "Action=purge_reverse" ) == 0 )
      {
        feed_pump_reverse_request(5.0, FEED_PUMP_FULL_SPEED_PCT, false);    // Full speed for 5 seconds
      }
      else if ( strcmp( post_data, "Action=setup_feeding" ) == 0 )
      {
        set_app_state(APP_STATE_FEEDING_SETUP_0);
      }
      else if ( strcmp( post_data, "Action=feed_now" ) == 0 )
      {
        manual_feed_request();
      }
      else if ( strcmp( post_data, "Action=skip_feeding" ) == 0 )
      {
        skip_next_feedings();
      }
      else if ( strcmp( post_data, "Action=cancel_feeding" ) == 0 )
      {
        cancel_feedings();
      }
      else if ( strcmp( post_data, "Action=reboot" ) == 0 )
      {
        esp_restart();
      }
      break;
    
    // Response: Action=[next|cancel]
    case APP_STATE_CALIBRATE_SCALE_0:
      if ( strcmp( post_data, "Action=next" ) == 0 )
      {
        HX711_tare();
        set_app_state(APP_STATE_CALIBRATE_SCALE_1);
      }
      else
      {
        set_app_state(APP_STATE_IDLE);
      }
      break;

    // Response: Action=[finish|cancel]
    case APP_STATE_CALIBRATE_SCALE_1:
      if ( strcmp( post_data, "Action=finish" ) == 0 )
      {
        float scale_val = ((float)HX711_get_value(200)) / 20;    // Average 200 readings, divide by 20gram weight
        nvm_set_param_float(NVM_PARAM_SCALE_CAL_VAL_TO_GRAMS, scale_val);
        HX711_set_scale(scale_val);
      }

      set_app_state(APP_STATE_IDLE);
      break;

    // Response: Action=[next|cancel]
    case APP_STATE_CALIBRATE_TUBING_0:
      if ( strcmp( post_data, "Action=next" ) == 0 )
      {
        set_app_state(APP_STATE_CALIBRATE_TUBING_1);
      }
      else
      {
        set_app_state(APP_STATE_IDLE);
      }
      break;
      
    // Response: Action=[cancel]
    case APP_STATE_CALIBRATE_TUBING_1:
      if ( strcmp( post_data, "Action=cancel" ) == 0 )
      {
        set_app_state(APP_STATE_IDLE);
      }
      break;

    // Response: FeedCnt=#&Action=[next|cancel]
    case APP_STATE_FEEDING_SETUP_0:
    {
      if ( strcmp( post_data, "Action=next" ) == 0 )
      {
        HX711_tare();
        set_app_state(APP_STATE_FEEDING_SETUP_1);
      }
      else
      {
        set_app_state(APP_STATE_IDLE);
      }
      break;
    }

    // Response: FeedCnt=#&Action=[next|cancel]
    case APP_STATE_FEEDING_SETUP_1:
    {
      char *tok = strchr(post_data, '&');
      *tok++ = 0;
      int feed_cnt = atoi(strchr(post_data, '=') + 1);
      char *action = strchr(tok, '=') + 1;

      if ( strcmp( action, "next" ) == 0 )
      {
        s_pending_feeding_state = get_current_feed_state();
        s_pending_feeding_state.time_between_feedings_s = ( 24 * 60 * 60 );
        s_pending_feeding_state.feedings_remaining = feed_cnt;
        s_pending_feeding_state.weight_per_feeding_g = HX711_get_units(SCALE_OVERSAMPLING_CNT) / feed_cnt;
        
        // Get the now's time
        time_t next_feed_time;
        time(&next_feed_time);
        
        // Figure out the start time
        struct tm timeinfo = { 0 };
        localtime_r(&next_feed_time, &timeinfo);
        
        // If it's before 4:00 PM, schedule it for today, otherwise, schedule for tomorrow
        if ( timeinfo.tm_hour < ( 12 + 4 ) )
        {
            timeinfo.tm_sec = -s_pending_feeding_state.time_between_feedings_s;
        }

        // All feedings will be at 7:30 PM
        timeinfo.tm_hour = 12 + 7;
        timeinfo.tm_min = 30;
        timeinfo.tm_sec += s_pending_feeding_state.time_between_feedings_s;
        
        // Convert back to unix time
        s_pending_feeding_state.next_scheduled_feed_time_s = mktime(&timeinfo);  
        
        set_app_state(APP_STATE_FEEDING_SETUP_2);
      }
      else
      {
        set_app_state(APP_STATE_IDLE);
      }
      break;
    }
    
    // Response: Action=[finish|cancel]
    case APP_STATE_FEEDING_SETUP_2:
      if ( strcmp( post_data, "Action=finish" ) == 0 )
      {
        set_current_feed_state( s_pending_feeding_state );

        set_app_state(APP_STATE_FEEDING_SCHEDULED);
      }
      else
      {
        set_app_state(APP_STATE_IDLE);
      }
      break;
      
    default:
      break;
  }

  _get_app_html( s_scratch_buffer );

  httpd_resp_set_status( req, HTTPD_200 );
  httpd_resp_send( req, s_scratch_buffer, strlen( s_scratch_buffer ) );

  return ESP_OK;
}


//-----------------------------------------------------------------------------
const char html_post_ota_file[] = "\
<style>\n\
.progress {margin: 15px auto;  max-width: 500px;height: 30px;}\n\
.progress .progress__bar {\n\
  height: 100%; width: 1%; border-radius: 15px;\n\
  background: repeating-linear-gradient(135deg,#336ffc,#036ffc 15px,#1163cf 15px,#1163cf 30px); }\n\
 .status {font-weight: bold; font-size: 30px;};\n\
</style>\n\
<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.2.1/css/bootstrap.min.css\">\n\
<div class=\"well\" style=\"text-align: center;\">\n\
  <div class=\"btn\" onclick=\"file_sel.click();\"><i class=\"icon-upload\" style=\"padding-right: 5px;\"></i>Upload Firmware</div>\n\
  <div class=\"progress\"><div class=\"progress__bar\" id=\"progress\"></div></div>\n\
  <div class=\"status\" id=\"status_div\"></div>\n\
</div>\n\
<input type=\"file\" id=\"file_sel\" onchange=\"upload_file()\" style=\"display: none;\">\n\
<script>\n\
function upload_file() {\n\
  document.getElementById(\"status_div\").innerHTML = \"Upload in progress\";\n\
  let data = document.getElementById(\"file_sel\").files[0];\n\
  xhr = new XMLHttpRequest();\n\
  xhr.open(\"POST\", \"/ota\", true);\n\
  xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');\n\
  xhr.upload.addEventListener(\"progress\", function (event) {\n\
     if (event.lengthComputable) {\n\
    	 document.getElementById(\"progress\").style.width = (event.loaded / event.total) * 100 + \"%\";\n\
     }\n\
  });\n\
  xhr.onreadystatechange = function () {\n\
    if(xhr.readyState === XMLHttpRequest.DONE) {\n\
      var status = xhr.status;\n\
      if (status >= 200 && status < 400)\n\
      {\n\
        document.getElementById(\"status_div\").innerHTML = \"Upload accepted. Device will reboot.\";\n\
      } else {\n\
        document.getElementById(\"status_div\").innerHTML = \"Upload rejected!\";\n\
      }\n\
    }\n\
  };\n\
  xhr.send(data);\n\
  return false;\n\
}\n\
</script>";

//-----------------------------------------------------------------------------
static esp_err_t _http_ota_get_handler( httpd_req_t *req )
{
  basic_auth_info_t *basic_auth_info = req->user_ctx;

  size_t buf_len = httpd_req_get_hdr_value_len( req, "Authorization" ) + 1;
  if ( ( buf_len > 1 ) && ( buf_len <= sizeof( s_scratch_buffer ) ) )
  {
    if ( httpd_req_get_hdr_value_str( req, "Authorization", s_scratch_buffer, buf_len ) == ESP_OK )
    {
      char *auth_credentials = _http_auth_basic( basic_auth_info->username, basic_auth_info->password );
      if ( !strncmp( auth_credentials, s_scratch_buffer, buf_len ) )
      {
        printf( "Authenticated!\n" );
        httpd_resp_set_status( req, HTTPD_200 );
        httpd_resp_set_hdr( req, "Connection", "keep-alive" );
        httpd_resp_send( req, html_post_ota_file, strlen( html_post_ota_file ) );
        return ESP_OK;
      }
    }
  }

  printf( "Not authenticated\n" );
  httpd_resp_set_status( req, "401 UNAUTHORIZED" );
  httpd_resp_set_hdr( req, "Connection", "keep-alive" );
  httpd_resp_set_hdr( req, "WWW-Authenticate", "Basic realm=\"Hello\"" );
  httpd_resp_send( req, NULL, 0 );

  return ESP_OK;
}

//-----------------------------------------------------------------------------
static esp_err_t _http_ota_post_handler( httpd_req_t *req )
{
  char buf[256];
  httpd_resp_set_status( req, HTTPD_500 );    // Assume failure
  
  int ret, remaining = req->content_len;
  printf( "Receiving\n" );
  
  esp_ota_handle_t update_handle = 0 ;
  const esp_partition_t *update_partition = esp_ota_get_next_update_partition(NULL);
  const esp_partition_t *running          = esp_ota_get_running_partition();
  
  if ( update_partition == NULL )
  {
    printf( "Uh oh, bad things\n" );
    goto return_failure;
  }

  printf( "Writing partition: type %d, subtype %d, offset 0x%08x\n", update_partition-> type, update_partition->subtype, update_partition->address);
  printf( "Running partition: type %d, subtype %d, offset 0x%08x\n", running->type,           running->subtype,          running->address);
  esp_err_t err = ESP_OK;
  err = esp_ota_begin(update_partition, OTA_WITH_SEQUENTIAL_WRITES, &update_handle);
  if (err != ESP_OK)
  {
      printf( "esp_ota_begin failed (%s)", esp_err_to_name(err));
      goto return_failure;
  }
  while ( remaining > 0 )
  {
    // Read the data for the request
    if ( ( ret = httpd_req_recv( req, buf, MIN( remaining, sizeof( buf ) ) ) ) <= 0 )
    {
      if ( ret == HTTPD_SOCK_ERR_TIMEOUT )
      {
        // Retry receiving if timeout occurred
        continue;
      }

      goto return_failure;
    }
    
    size_t bytes_read = ret;
    
    remaining -= bytes_read;
    err = esp_ota_write( update_handle, buf, bytes_read);
    if (err != ESP_OK)
    {
      goto return_failure;
    }
  }

  printf( "Receiving done\n" );

  // End response
  if ( ( esp_ota_end(update_handle)                   == ESP_OK ) && 
       ( esp_ota_set_boot_partition(update_partition) == ESP_OK ) )
  {
    printf( "OTA Success?!\n Rebooting\n" );
    fflush( stdout );

    httpd_resp_set_status( req, HTTPD_200 );
    httpd_resp_send( req, NULL, 0 );
    
    vTaskDelay( 2000 / portTICK_RATE_MS);
    esp_restart();
    
    return ESP_OK;
  }
  printf( "OTA End failed (%s)!\n", esp_err_to_name(err));

return_failure:
  if ( update_handle )
  {
    esp_ota_abort(update_handle);
  }

  httpd_resp_set_status( req, HTTPD_500 );    // Assume failure
  httpd_resp_send( req, NULL, 0 );
  return ESP_FAIL;
}

//-----------------------------------------------------------------------------
static httpd_handle_t _start_webserver( void )
{
  httpd_handle_t server = NULL;
  httpd_config_t config = HTTPD_DEFAULT_CONFIG();
  config.lru_purge_enable = true;

  // Start the httpd server
  printf( "Starting server on port %d\n", config.server_port );

  httpd_handle_t http_server = NULL;
  if ( httpd_start( &http_server, &config ) == ESP_OK )
  {
    static httpd_uri_t root =
    {
      .uri       = "/",
      .method    = HTTP_GET,
      .handler   = _http_app_get_handler,
      .user_ctx  = NULL,
    };
    httpd_register_uri_handler( http_server, &root );

    static const httpd_uri_t ota =
    {
      .uri       = "/",
      .method    = HTTP_POST,
      .handler   = _http_app_post_handler,
      .user_ctx  = NULL
    };
    httpd_register_uri_handler( http_server, &ota );
    
    
    static basic_auth_info_t ota_auth_info = 
    {
      .username = "kevin",
      .password = "letmein",
    };

    static httpd_uri_t ota_get =
    {
      .uri       = "/ota",
      .method    = HTTP_GET,
      .handler   = _http_ota_get_handler,
      .user_ctx  = &ota_auth_info,
    };
    httpd_register_uri_handler( http_server, &ota_get );

    static const httpd_uri_t ota_post =
    {
      .uri       = "/ota",
      .method    = HTTP_POST,
      .handler   = _http_ota_post_handler,
      .user_ctx  = NULL
    };
    httpd_register_uri_handler( http_server, &ota_post );
  }
    
  return server;
}

//-----------------------------------------------------------------------------
static void _connect_handler( void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data )
{
  s_wifi_connected = true;
  
  tcpip_adapter_ip_info_t ipInfo; 
  tcpip_adapter_get_ip_info(TCPIP_ADAPTER_IF_STA, &ipInfo);
  
  sprintf( s_ip_addr_str, "%i.%i.%i.%i", 
    ( ipInfo.ip.addr >>  0 ) & 0xFF,
    ( ipInfo.ip.addr >>  8 ) & 0xFF,
    ( ipInfo.ip.addr >> 16 ) & 0xFF,
    ( ipInfo.ip.addr >> 24 ) & 0xFF );

  printf("Wifi connected, IP: %s\n", s_ip_addr_str);
}

//-----------------------------------------------------------------------------
static void _disconnect_handler( void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data )
{
  printf( "Disconnect?\n" );
  s_wifi_connected = false;
}

//-----------------------------------------------------------------------------
void _time_sync_notification_cb(struct timeval *tv)
{
  s_ntp_time_set = true;
  printf( "Date/Time updated at: %s\n", get_system_time_str() );
}

//-----------------------------------------------------------------------------
static void _wifi_task( void *Param )
{
  printf( "Wifi task starting\n" );
   
  httpd_handle_t server = NULL;
  
  // Register event handlers to stop the server when Wi-Fi is disconnected, and re-start it upon connection
  esp_event_handler_register( IP_EVENT, IP_EVENT_STA_GOT_IP, &_connect_handler, &server );
  esp_event_handler_register( WIFI_EVENT, WIFI_EVENT_STA_DISCONNECTED, &_disconnect_handler, &server );
  
  esp_netif_init();
  example_connect();
  server = _start_webserver();
  
  esp_mqtt_client_config_t mqtt_cfg =
  {
      .uri = "mqtt://192.168.0.41",
  };

  esp_mqtt_client_handle_t client = esp_mqtt_client_init(&mqtt_cfg);
  esp_mqtt_client_register_event(client, ESP_EVENT_ANY_ID, mqtt_event_handler, NULL);
  esp_mqtt_client_start(client);
  
  sntp_setoperatingmode(SNTP_OPMODE_POLL);
  sntp_setservername(0, "pool.ntp.org");
  sntp_set_time_sync_notification_cb(_time_sync_notification_cb);
  sntp_init();
  
  const uint32_t task_delay_ms = 100;
  while(1)
  {    
    vTaskDelay( task_delay_ms / portTICK_RATE_MS);
    
    static float last_status_update = 0;
    static float last_ip_update = 0;
    
    if ( s_force_mqtt_updates )
    {
      last_status_update = 0;
      last_ip_update     = 0;
      s_force_mqtt_updates = false;
      
      // Give some time for other tasks to process any requests
      vTaskDelay( 500 / portTICK_RATE_MS);
    }
    
    uint32_t status_update_rate_s = ( get_app_state() == APP_STATE_FEEDING_IN_PROGRESS ? 1 : 5 );
    if ( ( last_status_update == 0 ) || ( ( system_uptime_s() - last_status_update ) > status_update_rate_s ) )
    {
      static char status_msg[256];
      static uint32_t status_msg_id = 0;
      
      feeding_state_t feed_state = get_current_feed_state();

      char *p_msg = status_msg;
      status_msg_id++;
      p_msg += sprintf( p_msg, "{ \"message_id\":%i,",    status_msg_id );
      p_msg += sprintf( p_msg, "\"app_state\":\"%s\",",   get_app_state_str() );     // 'state' is a reserved name in Home Assistant
      p_msg += sprintf( p_msg, "\"uptime\":%u,",          (uint32_t)system_uptime_s() );
      
      p_msg += sprintf( p_msg, "\"system_time\":\"%s\",", get_system_time_str() );
      
      p_msg += sprintf( p_msg, "\"next_feed_time\":\"");
      if ( feed_state.feedings_remaining )
        p_msg += add_formatted_timestamp( p_msg, feed_state.next_scheduled_feed_time_s );
      else
        p_msg += sprintf( p_msg, "None Scheduled");
      p_msg += sprintf( p_msg, "\",");
      
      p_msg += sprintf( p_msg, "\"last_feed_time\":\"");
      p_msg += add_formatted_timestamp( p_msg, feed_state.last_feed_time_s );
      p_msg += sprintf( p_msg, "\",");
      
      p_msg += sprintf( p_msg, "\"feeds_remaining\":%u,", feed_state.feedings_remaining );
      p_msg += sprintf( p_msg, "\"temp_f\":%0.1f,",       get_latest_temp_reading_f());
      p_msg += sprintf( p_msg, "\"scale_reading_g\":%li", get_latest_scale_reading_g());
      p_msg += sprintf( p_msg, "}" );
      
      esp_mqtt_client_publish( client, "reef/frozen_feeder/status", status_msg, 0, 1, 0);
      last_status_update = system_uptime_s();
    }
    
    if ( ( last_ip_update == 0 ) || ( ( system_uptime_s() - last_ip_update ) > 60 ) )
    {
      char ip_msg[48];
      sprintf( ip_msg, "{ \"ip_addr\":\"%s\"}", s_ip_addr_str );
      esp_mqtt_client_publish( client, "reef/frozen_feeder/ip", ip_msg, 0, 1, 0);
      last_ip_update = system_uptime_s();
    }
  }
}

//-----------------------------------------------------------------------------
static void _ota_task(void *Param)
{
  printf( "OTA task starting\n" );
  const esp_partition_t *running = esp_ota_get_running_partition();
  esp_ota_img_states_t ota_state;
  if ( esp_ota_get_state_partition(running, &ota_state) == ESP_OK )
  {
    if (ota_state == ESP_OTA_IMG_PENDING_VERIFY)
    {
      // Validate image some how, then call:
      esp_ota_mark_app_valid_cancel_rollback();
      // If needed: esp_ota_mark_app_invalid_rollback_and_reboot();
    }
  }
  
  const uint32_t task_delay_ms = 10;
  while(1)
  {
    vTaskDelay( task_delay_ms / portTICK_RATE_MS);
  }
}

//-----------------------------------------------------------------------------
void wifi_init( void )
{
  xTaskCreate( _wifi_task, "_wifi_task", 4096, NULL, 0, NULL );
  xTaskCreate( _ota_task, "_ota_task", 8192, NULL, 5, NULL);
}
