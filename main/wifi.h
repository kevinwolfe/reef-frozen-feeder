#ifndef _WIFI_H_
#define _WIFI_H_

#include <stdbool.h>

void wifi_init( void );
bool ntp_time_is_set();
const char *get_ip_addr_str();
void force_mqtt_update( void );

#endif